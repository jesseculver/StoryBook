A simple gamemode for Garry's Mod that is aimed at people who wish to make maps
that are more narrative focused and don't want their players to have access to
sandbox tools.

This gamemode does *not* add new content

A .FGD file is included that you should add to your Hammer configuration so that you
can modify values